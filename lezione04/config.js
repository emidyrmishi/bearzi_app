/******************************************
 *  Author : Author   
 *  Created On : Wed Nov 07 2018
 *  File : config.js
 *******************************************/

 var nome =  "Emi";
 var eta = 27;

 console.log(nome +" "  + eta );
 console.log("Tra 10 anni " + nome + " avra "  + (eta + 10) );
 console.log("Eta / 2 = " + (eta / 2));
 console.log("Eta % 2 = " + (eta % 2));
 console.log("Eta ++ = " + (++eta));
 console.log("Eta -- = " + (--eta));
 console.log("Eta += 20 = " + (eta+=20));
 console.log("-------------------------------------------------------");
 console.log("!true = " + (!true));
 console.log("1 == 2 : " + (1==2));
 console.log("1 != 2 : " + (1!=2));
 console.log("1 === '1' : " + (1==="1"));
 console.log("1 == 2 && 2 == 2 : " + (1==2 && 2==2));
 console.log("1 == 1 && 2 == 2 : " + (1==1 && 2==2));
 