/******************************************
 *  Author : Author   
 *  Created On : Wed Nov 07 2018
 *  File : test.js
 *******************************************/

console.log("Hello world" );
console.info("Questo è un info" );
console.warn("Questo è un worn" );
console.error("Questo è un errore" );

var ogetto = {
    nome : "a",
    tipo : "ogetto numerico",
    valore : 127,
    valori :  [
        1,2,3,4,5
    ],
    proprieta : {
        prop1 : 1,
        prop2 : 5
    }
};

//alert ("si e verificato un errore");
console.log(ogetto);

console.log("-----------------------------------------------------");

var a = 4;

switch(a){
    case 1:
        alert("a è uguale a 1");
        break;
    case 2:
        alert("a è uguale a 2");
        break;
    case 3: 
        alert("a è uguale a 3");
        break;
    case 4:    
        alert("a è uguale a 4");
        break;
    default:
        alert("a è uguale a 5");
}
