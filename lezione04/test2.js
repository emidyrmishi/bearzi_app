///////////////////////////////////Array//////////////////////////////////////
var colori = [
    "Rosso", "Giallo", "Verde"
]
var i = 0 ;
console.log("posizione 0 = " + colori[0]);
console.log("posizione 1 = " + colori[1]);
console.log("posizione 2 = " + colori[2]);
console.log("posizione 0 = " + colori);
console.log("Arrey completa: " + colori);
colori.push(150);
console.log("Arrey completa + numero: " + colori);
console.log("tipo val 0 = : " + typeof colori[0]);
console.log("tipo val 3 = : " + typeof colori[3]);
console.log("----------------------------------");

colori.push([1, 2, 4, 5])
console.log("Nasted Arrey : ");
console.log(colori)

console.log("----------------------------------");

for(i= 0; i<colori.length; i++){
    console.log("All array for loop [i]" + colori[i])
    if(colori[i] == "Verde"){
        console.log("Trovato colore Verde");
    }
}