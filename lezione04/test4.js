


var me = {
    nome :  "Emi",
    cognome : "Dyrmishi",
    eta :  43,
    "sport preferiti" : [
        "Nuoto",
        "Snowboard"
    ]
};

console.log(me);
console.log("Nome: " + me.nome);
console.log("Nome: " + me["sport preferiti"]);
me.sesso =  "Maschile";
console.log(me);
me.eta = 27;
console.log(me);

if( me.eta > 45){
    console.log(me.nome + " ha piu di 45 anni");
}else{
    console.log(me.nome + " " + me.cognome +  " ha meno di 45 anni");
}

if( me.sesso != "Maschile ")
    console.log(me.nome + " non è un uomo");

if(typeof me2 !== "undefined"){
    console.log("me2 e definita");
}    

if(typeof me !== "undefined"){
    console.log("me e definita");
}