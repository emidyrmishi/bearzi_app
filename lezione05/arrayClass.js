/******************************************
 *  Author : Author   
 *  Created On : Sat Nov 10 2018
 *  File : arrayClass.js
 *******************************************/

var fruits = ["Banana", "Orange", "Apple", "Mango"];
var energy = fruits.join(" - ");
console.log(energy);
var frase = "Frase un po lunga da invertire con javascript";
var invertita = frase.split("").reverse().join("");
console.log(frase);
console.log(invertita);
var arr1 = ["Rosso", "Verde", "Giallo", "nero"];
var arr2 =  arr1.join("/").split("/")
arr2.reverse();
console.log(arr1);
console.log(arr2);
console.log(frase.charAt(1));


console.log(frase.indexOf("javascript"));
console.log(frase.replace("un po", "troppo"));

var frase2= frase;
var changed = true;

while(changed){
    var temp = frase2.replace("a","?");
    if(temp == frase2){
        changed = false;
    }else{
        frase2 = temp;
    }
}

console.log(frase);
console.log(frase2);