function contaLettere(frase){
    //trasformo la frase in minuscolo
    frase= frase.toLowerCase();
    //trasformo la frase in array di caratteri singoli
    var arr_caratteri = frase.split("");
    //creo un hash dove salvare il contattore di ogni lettera trovato
    var risultato = {};
    //definisco l'array dei caratteri amessi
    var amessi = "abcdefghilmnopqrstuyxwvzèàò".split("")
    console
    //faccio un loop su tutte le lettere 
    for(var i= 0 ; i<arr_caratteri.length; i++){
        //salvo in una variabile temporania la lettera corrente
        var lettera = arr_caratteri[i];
        if(amessi.indexOf(lettera)>=0){
              //aumento il contatore della lettera trovata
            if(risultato[lettera]){
                risultato[lettera] += 1;
            }else{
                risultato[lettera] = 1;
            }
        }
    }
    //stampo il resultato
    return risultato;
}
function stampaInConsole(oggeto){
    for(var i in oggeto){
        var contatore = oggeto[i];
        console.log("la lettera "+ i +" compare " + contatore + " volte nella frase")
    }
}

function stampaInHtml(oggeto){
    var temp_arr = [];
    for(var i in oggeto){
        var contatore = oggeto[i];
        temp_arr.push("la lettera "+ i +" compare " + contatore + " volte nella frase");
    }
    document.write("<p>" + temp_arr.join("</p><p>") + "</p>");
}
//definisco la frase da controllare
var frase = "Buongiorno, questa è la sesta lezione al bearzi";
//richiamo la funzione
var risultato = contaLettere(frase); 
stampaInConsole(risultato);
stampaInHtml(risultato);
console.log(risultato);