class Being{
    constructor(){
        console.log("i am a being");
    }
    seiHi(){
        console.log("Hi");
        //this._metodoPrivato();
    }
    _metodoPrivato(){
        console.log("privato");
    }
}

class Animal extends Being{
    constructor(age){
        super();
        this.age = age;
        this.favorite_food = "";
        this.noise = "";
        /*var _this = this;
        setTimeout(function(){
            _this.makeNoise();
        },1000);*/
        setTimeout(()=>this.makeNoise(),1000);
        console.log("I am an Animal and i am: " + this.age+ " years old")
    }

    run(){
        console.log("I Am Running");
    }
    eat(){
        console.log("I eat " + this.favorite_food);
    }
    makeNoise(){
        console.log(this.noise);
    }
}
class Dog extends Animal{
        constructor(age){
            super(age);
            this.favorite_food = "Meat";
            this.noise = "bau";
            this._noiseInterval = setInterval(()=>this.makeNoise(),2000);
            console.log("I am a Dog and i am: " + this.age+ " years old")
        }
    shhht(){
        clearInterval(this._noiseInterval);
    }    
}
class Cat extends Animal{
    constructor(age){
        super(age);
        this.favorite_food = "Fish";
        this.noise = "miao";
        console.log("I am a Cat and i am: " + this.age+ " years old")
    }
    run(){
        console.log("I am like a cat");
    }
}


var b = new Being();
b.seiHi()
console.log("-------------------------")
var a = new Animal(5);
console.log(a.age);
var dog = new Dog(2);
dog.run();
dog.eat();
dog.makeNoise();
console.log("-------------------------")
var cat = new Cat(3);
cat.run();
cat.eat();
cat.makeNoise();