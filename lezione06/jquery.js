function cambiaColorePrimoLi(colore){
    if(typeof colore === "object") {
        colore = colore.data;
    }
    //$("li.primo").css("color",colore); 
    $("li.primo").css({
        "color": colore,
        background: "#999",
        fontSize: "24px"
    }); 
    return false;
}
function addClassLG(){
    $("div#contenitore").addClass("lg");
    return false;
}
function toggleClassLG(){
    $("div#contenitore").toggleClass("lg");
    return false;
}

function spostaElemento(){
    var elemento = $("#ul1").find(".ultimo").detach();
    $("#ul2").prepend(elemento);
    return false;
}
function riempiDescrizione(){
    $(".descrizione").html("Testo completto della descrizione");
}

function nascondiUl(){
    $("ul").hide(1000);
}

function visualizzaUl(){
    $("ul").show(1000);
}

$( document).ready(function(){
    var pulsante2 = $("a#pulsante2");
    pulsante2.click(function(){
        cambiaColorePrimoLi("green");
        return false;
    });
    $("a#pulsante3").bind("click","blue",cambiaColorePrimoLi);
    $("a#pulsante4").click("yellow",cambiaColorePrimoLi);
    //aggiungo un pulsante
    $("div#contenitore").append("<a id='pulsante6' href='#'>Aggiungi classe lg</a>");
    $("a#pulsante6").click(addClassLG);
    $("div#contenitore").append("<a id='pulsante7' href='#'>Toggle classe lg</a>");
    $("a#pulsante7").click(toggleClassLG);
    $("div#contenitore").append("<a id='pulsante8' href='#'>Sposta elemento</a>");
    $("a#pulsante8").click(spostaElemento);
    $("div#contenitore").append("<a id='pulsante9' href='#'>Riempi descrizione</a>");
    $("a#pulsante9").click(riempiDescrizione);
    $("div#contenitore").append("<a id='pulsante10' href='#'>Nascondi ul</a>");
    $("a#pulsante10").click(nascondiUl);
    $("div#contenitore").append("<a id='pulsante11' href='#'>Visualizza ul</a>");
    $("a#pulsante11").click(visualizzaUl);
});

