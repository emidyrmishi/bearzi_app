/******************************************
 *  Author : Author   
 *  Created On : Wed Nov 14 2018
 *  File : test.js
 *******************************************/


class MyClass {
    constructor(field1) {
        this.field1 = field1;
    }
    get getField1() {
        return this.field1;
    }
    get getField2() {
        return this.field2;
    }
    set setField1(field1) {
        this.field1 = field1;
    }
    set setField2(field2){
        this.setField2 = field2;
    }
    summ(field1, field2){
        this.field1 = field1; 
        this.field2 = field2;
        var risult = this.field1 + this.field2;
        return risult;
    } 
}
var myClass = new MyClass("1");
console.log(myClass.field1); // => 1
myClass.setField1 = 2;
console.log(myClass.getField1); // => 2
console.log(myClass.summ(2,4));