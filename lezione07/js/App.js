

class App{
    constructor(){
        this.gara = null;
        this._stato =null;

        $(document).ready(()=>{
            this._init();
        });

    }
    /**
     * utilizza GaraVo 
     */
       
    _init(){
        //creo la gara
        this.gara = new Gara(100);
        this.gara.generaAtleti(4);
        //popolo la pagina
        this.stampaAtleti();
        //imposto lo stato iniziale
        this._setStato("home");
        // TODO: togliere
        this._setStato("gara");
        //collego i pulsanti
        this._initPulsanti();
    }
    _initPulsanti(){
        $("#stato-home #crea").bind("click", "crea-gara",(evento)=>this._clickHandler(evento));
        $("#azioni #parti").bind("click", "parti",(evento)=>this._clickHandler(evento));
    }
    /**
     * Gestisce tutti i click dell'applicazione
     * @param {Event} evento 
     */
    _clickHandler(evento){
        switch(evento.data){
            case "crea-gara":
            this._setStato("gara");
            break;
            //richiama la funzione partenza di gara passandoli i due callback
            case "parti":
            this.gara.partenza(()=>this.aggornaRisultato(),()=>this.stampaRisulato());
            break;
            default:
            alert("click non previsto");
            break;
        }
    }

    aggornaRisultato(){
        //salvo l'array in una variabile locale
        var atleti = this.gara.gara_vo.atleti;
        for(var i = 0 ; i< atleti.length;i++){
            var atleta = atleti[i];
            //calcola la percentuale del avanzamento 
            var perc = Math.round((atleta.distanzaPercorsa / this.gara.gara_vo.distanza)*1000)/10;
            //recupero il div del atleta
            var div_atleta =  $("#atleta-"+ atleta.id);
            var barra = div_atleta.find(".barra");
            //aggiorno la largezza della barra
            barra.css("width", perc + "%");
        }
    }

    /**
     * sceglie la pagina da visualizzare
     * @param {String} stato 
     */
    _setStato(stato){
        //aggiorno la variabile d'istanza
        this._stato = stato;
        var id_div;
        switch (stato){
            case "home":
                id_div= "stato-home";
                break;
            case "gara":
                id_div= "stato-gara";
                break;
        }
        $(".stato").css("display","none");
        if(id_div) $("#"+id_div).css("display","block");

    }

    stampaAtleti(){
        //popolare la durata
        var template = $("#template-atleta").html();
        var contenitore =  $("#atleti");
        for(var i= 0;i<this.gara.gara_vo.atleti.length;i++){
            var atleta = this.gara.gara_vo.atleti[i];
            var html = template.replace("***nome***", atleta.nome);
             html = html.replace("***eta***", atleta.eta);
             html = html.replace("***sesso***", atleta.sesso);
             html = html.replace("***id***", atleta.id);
            contenitore.append(html);
        }
        //creare div atleti
    }

    stampaRisulato(gara){

    }
}

