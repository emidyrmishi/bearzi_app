class Atleta extends Persona{
    constructor(nome,eta,sesso,distanza,callbackArrivato){
        //richiamo il costrutore del padre passando i parametri ricevuti
        super(nome,eta,sesso);
        //assegno la proprieta d'istanza
        
        this.velocita = this._calcolaVelocita();;
        this.callbackArrivato= callbackArrivato;
        this.distanzaTotale = distanza;
        this.distanzaPercorsa = 0;
        this.posizione = null;
        this.id = ++Atleta.tot_atleti;
        this.arrivato = false;
        

    }
    corri(secondi){
        if(this.arrivato) return;
        this.distanzaPercorsa = secondi * this.velocita;  
        if(this.distanzaPercorsa>=this.distanzaTotale){
            this.arrivato= true;
            this.callbackArrivato();
        }
    }
    _calcolaVelocita(){
        var velocita = 6 ;
        var svantaggioEta = (this.eta - 15) / (45) * 2 ;
        var svantaggioSesso = (this.sesso == "Femmina") ? 0.3 : 0;
        var svantaggioCasuale =  Math.random()/2;
        velocita -=  svantaggioEta ;
        velocita -=  svantaggioSesso;
        velocita -=  svantaggioCasuale;
        console.log(this.nome +" - "+ this.eta + " velocita " + velocita);
        return velocita;
    }
}
//definisco una proprieta statica
Atleta.tot_atleti = 0;