class Gara{
    constructor(distanza){
        //instanzio le proprieta di istanza 
        this.gara_vo = new GaraVO();
        this.gara_vo.distanza = distanza;
        this.callbackArrivato = null;
        this.callbackUpdate = null;
        this._interval_gara = null;
        this.ts_partenza = null;
        this._poolAtleti= [
            {nome:"Mario Rossi", sesso:"Maschio"},
            {nome:"Luigi Bianchi", sesso:"Maschio"},
            {nome:"Alberto Soldi", sesso:"Maschio"},
            {nome:"Mara Bianconi", sesso:"Femmina"},
            {nome:"Guido Rigoni", sesso:"Maschio"},
            {nome:"Anna Tatangelo", sesso:"Femmina"},
            {nome:"Elisa Giano", sesso:"Femmina"},
            {nome:"Amanda RosLearsi", sesso:"Femmina"},
            {nome:"Giovani Petta", sesso:"Maschio"}
        ];
    }
    
    /**
     * crea un atleta vo e la salva nel GaraVo
     * @param {Numero} num_atleti 
     * @returns{Atleta}
     */
    generaAtleti(num_atleti){
       var arr_atleti = [];
       //creiamo il callback fuori dal loop per generare una sola funzione
       var callbackArrivato = ()=>this._arrivato();
       //creiamo una copia del array
       var poolAtleti = JSON.parse(JSON.stringify(this._poolAtleti));

       for(var i= 0;i<num_atleti;i++){
            var posizioneCasuale =Utilis.randRange(0,poolAtleti.length-1) ;
            var infoAtleta = poolAtleti.splice(posizioneCasuale,1)[0];
            var eta = Utilis.randRange(15,60);
            var atleta = new Atleta(infoAtleta.nome, eta, infoAtleta.sesso, this.gara_vo.distanza, callbackArrivato);
            arr_atleti.push(atleta);
       }

       //aggiorno GaraVO
       this.gara_vo.atleti = arr_atleti;
       return arr_atleti;

    }
    _arrivato(){
        //callback richiama quando l'atleta è arrivato al traguardo
        console.log("Arrivato");
        //salvo i atleti in una variabile locale
        var atleti = this.gara_vo.atleti;
        var atletiArrivati = 0;
        for(var i= 0 ;i < atleti.length;i++){
            var atleta = atleti[i];
            if(atleta.arrivato) atletiArrivati++;
        }
        if(atletiArrivati>=atleti.length){
            this._fineGara();
        }
    }

    _fineGara(){
        console.log("fine gara");
        clearInterval(this._interval_gara);
        this._interval_gara = null;
    }
    partenza(callbackUpdate,callbackFineGara){
        this.callbackUpdate = callbackUpdate;
        this.callbackFineGara = callbackFineGara;
        //salvo una variabile di'istanza nel momento corrente
        this._ts_partenza = Date.now();
       
        //TODO: togliere
        this._interval_gara = setInterval(()=>this._avanzamentoTempo(),10);
    }
    
    _avanzamentoTempo(){
        //calcolo quanti secondi sono passati
        var ts_now = Date.now();
        var ms_trascorsi = ts_now - this._ts_partenza;
        var tempoTrascorso = ms_trascorsi/100;
        //loop tra i atleti per vedere dove sono arrivati 
        for(var i = 0 ; i<this.gara_vo.atleti.length;i++){
            var atleta = this.gara_vo.atleti[i];
            atleta.corri(tempoTrascorso);
        }
        //richiamiamo il callback
        this.callbackUpdate();
    }

}